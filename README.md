# Moderation State Transitions Widget

An alternative widget for the core Moderation State to allow editors to select
from available transitions, instead of state names. Widget settings includes if
the form element should be select or radios, and if the "transition" where the
state does not change should be presented as an option in the UI at all.

## Dependencies

Depends only on the Content Moderation (content_moderation) module of
Drupal Core.
