<?php

namespace Drupal\Tests\moderation_state_transitions_widget\Kernel;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormState;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\RandomGeneratorTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\moderation_state_transitions_widget\Plugin\Field\FieldWidget\ModerationStateTransitionsWidget;

/**
 * @coversDefaultClass \Drupal\moderation_state_transitions_widget\Plugin\Field\FieldWidget\ModerationStateTransitionsWidget
 * @group moderation_state_transitions_widget
 */
class ModerationStateTransitionsWidgetTest extends KernelTestBase {

  use ContentModerationTestTrait;
  use RandomGeneratorTrait;
  use UserCreationTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'workflows',
    'content_moderation',
    'node',
    'moderation_state_transitions_widget',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->installEntitySchema('content_moderation_state');
    $this->installEntitySchema('user');
    $this->installConfig(['content_moderation', 'system']);

    NodeType::create([
      'type' => 'moderated',
    ])->save();
    NodeType::create([
      'type' => 'unmoderated',
    ])->save();

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'moderated');
    $workflow->save();

    // Setup an admin user for testing.
    $this->setUpCurrentUser([], [], TRUE);
  }

  /**
   * Test the widget on a moderated entity form.
   */
  public function testWidgetModeratedEntity() : void {
    // Create a  oderated entity and build a form display which will include
    // the ModerationStateWidget plugin.
    $entity = Node::create([
      'type' => 'moderated',
    ]);
    $entity_form_display = EntityFormDisplay::create([
      'targetEntityType' => 'node',
      'bundle' => 'moderated',
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $entity_form_display->setComponent('moderation_state', [
      'type' => 'moderation_state_transitions',
    ])->save();

    $form = [];
    $form_state = new FormState();
    $entity_form_display->buildForm($entity, $form, $form_state);

    // Check the default widget behavior.
    $this->assertNotEmpty($form['moderation_state']['widget'][0]['state']);
    $this->assertEquals('select', $form['moderation_state']['widget'][0]['state']['#type']);
    $this->assertEquals('draft', $form['moderation_state']['widget'][0]['state']['#default_value']);
    $this->assertEquals('Create New Draft', $form['moderation_state']['widget'][0]['state']['#options']['draft']);

    // Change the widget setting to radios and try again.
    $entity_form_display->setComponent('moderation_state', [
      'type' => 'moderation_state_transitions',
      'settings' => [
        'form_element' => 'radios',
        'default_value_label' => 'state',
      ],
    ])->save();

    $form = [];
    $form_state = new FormState();
    $entity_form_display->buildForm($entity, $form, $form_state);
    $this->assertEquals('radios', $form['moderation_state']['widget'][0]['state']['#type']);
    $this->assertEquals('draft', $form['moderation_state']['widget'][0]['state']['#default_value']);
    $this->assertEquals('Draft', $form['moderation_state']['widget'][0]['state']['#options']['draft']);

    // Change the widget setting to use a custom label and try again.
    $custom_label = $this->randomMachineName();
    $entity_form_display->setComponent('moderation_state', [
      'type' => 'moderation_state_transitions',
      'settings' => [
        'form_element' => 'radios',
        'default_value_label' => 'custom',
        'default_value_label_custom' => $custom_label,
      ],
    ])->save();

    $form = [];
    $form_state = new FormState();
    $entity_form_display->buildForm($entity, $form, $form_state);
    $this->assertEquals('radios', $form['moderation_state']['widget'][0]['state']['#type']);
    $this->assertEquals('draft', $form['moderation_state']['widget'][0]['state']['#default_value']);
    $this->assertEquals($custom_label, $form['moderation_state']['widget'][0]['state']['#options']['draft']);
  }

  /**
   * Test the widget does not impact a non-moderated entity.
   */
  public function testWidgetNonModeratedEntity() : void {
    // Create an unmoderated entity and build a form display which will include
    // the ModerationStateWidget plugin, in a hidden state.
    $entity = Node::create([
      'type' => 'unmoderated',
    ]);
    $entity_form_display = EntityFormDisplay::create([
      'targetEntityType' => 'node',
      'bundle' => 'unmoderated',
      'mode' => 'default',
      'status' => TRUE,
    ]);
    $form = [];
    $form_state = new FormState();
    $entity_form_display->buildForm($entity, $form, $form_state);

    // The moderation_state field should have no values for an entity that isn't
    // being moderated.
    $entity_form_display->extractFormValues($entity, $form, $form_state);
    $this->assertEquals(0, $entity->moderation_state->count());
  }

  /**
   * @covers ::isApplicable
   */
  public function testIsApplicable() : void {
    // The moderation_state field definition should be applicable to our widget.
    $fields = $this->container->get('entity_field.manager')->getFieldDefinitions('node', 'test_type');
    $this->assertTrue(ModerationStateTransitionsWidget::isApplicable($fields['moderation_state']));
    $this->assertFalse(ModerationStateTransitionsWidget::isApplicable($fields['status']));
    // A config override should still be applicable.
    $field_config = $fields['moderation_state']->getConfig('moderated');
    $this->assertTrue(ModerationStateTransitionsWidget::isApplicable($field_config));
  }

}
