<?php

namespace Drupal\moderation_state_transitions_widget\Plugin\Field\FieldWidget;

use Drupal\content_moderation\Plugin\Field\FieldWidget\ModerationStateWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the 'moderation_state_transitions' widget.
 *
 * Drupal\content_moderation\Plugin\Field\FieldWidget\ModerationStateWidget
 * extension to provide a UI based on the names of the available transitions,
 * not the names of the states.
 *
 * @FieldWidget(
 *   id = "moderation_state_transitions",
 *   label = @Translation("Moderation state - transitions"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class ModerationStateTransitionsWidget extends ModerationStateWidget implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'default_value_label' => 'transition',
      'default_value_label_custom' => '',
      'form_element' => 'select',
      'show_default_value_radio' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['form_element'] = [
      '#type' => 'select',
      '#title' => $this->t('Form element'),
      '#default_value' => $this->getSetting('form_element'),
      '#required' => TRUE,
      '#options' => $this->getSettingFormElementOptions(),
    ];
    $element['show_default_value_radio'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show default value radio'),
      '#default_value' => $this->getSetting('show_default_value_radio'),
      '#states' => [
        'visible' => [
          ':input[name="fields[moderation_state][settings_edit_form][settings][form_element]"]' => [
            ['value' => 'radios'],
          ],
        ],
      ],
    ];
    $element['default_value_label'] = [
      '#type' => 'select',
      '#title' => $this->t('Default value label'),
      '#default_value' => $this->getSetting('default_value_label'),
      '#options' => $this->getSettingDefaultValueLabelOptions(),
    ];
    $element['default_value_label_custom'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom label'),
      '#default_value' => $this->getSetting('default_value_label_custom'),
      '#size' => 60,
      '#maxlength' => 128,
      '#states' => [
        'visible' => [
          ':input[name="fields[moderation_state][settings_edit_form][settings][default_value_label]"]' => [
            ['value' => 'custom'],
          ],
        ],
      ],
    ];
    return $element;
  }


  /**
   * Returns the available options for the 'Form element' setting.
   *
   * @return TranslatableMarkup[]
   *   An array of settings option labels, keyed by settings values.
   */
  protected function getSettingFormElementOptions() : array {
    return [
      'select' => $this->t('Select'),
      'radios' => $this->t('Radios'),
    ];
  }

  /**
   * Returns the available options for the 'Default value label' setting.
   *
   * @return TranslatableMarkup[]
   *   An array of settings option labels, keyed by settings values.
   */
  protected function getSettingDefaultValueLabelOptions() : array {
    return [
      'transition' => $this->t('Transition'),
      'state' => $this->t('State'),
      'custom' => $this->t('Custom'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Form element: @element', ['@element' => $this->getSettingFormElementOptions()[$this->getSetting('form_element')]]);
    $summary[] = $this->t('Default value label: @label', ['@label' => $this->getSettingDefaultValueLabelOptions()[$this->getSetting('default_value_label')]]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $original_entity = $items->getEntity();

    $default = $this->moderationInformation->getOriginalState($entity);

    // If the entity already exists, grab the most recent revision and load it.
    // The moderation state of the saved revision will be used to display the
    // current state as well determine the appropriate transitions.
    if (!$entity->isNew()) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $original_entity */
      $original_entity = $this->entityTypeManager->getStorage($entity->getEntityTypeId())->loadRevision($entity->getLoadedRevisionId());
      if (!$entity->isDefaultTranslation() && $original_entity->hasTranslation($entity->language()->getId())) {
        $original_entity = $original_entity->getTranslation($entity->language()->getId());
      }
    }
    // For a new entity, ensure the moderation state of the original entity is
    // always the default state. Despite the entity being unsaved, it may have
    // previously been set to a new target state, for example previewed entities
    // are retrieved from temporary storage with field values set.
    else {
      $original_entity->set('moderation_state', $default->id());
    }

    /** @var \Drupal\workflows\Transition[] $transitions */
    $transitions = $this->validator->getValidTransitions($original_entity, $this->currentUser);

    $transition_labels = [];
    $default_value = $items->value;
    foreach ($transitions as $transition) {
      $transition_to_state = $transition->to();
      $transition_labels[$transition_to_state->id()] = $transition->label();
      if ($default->id() === $transition_to_state->id()) {
        $default_value = $default->id();
        switch ($this->getSetting('default_value_label')) {
          case 'custom':
            // @todo This should be inline Twig.
            $transition_labels[$default_value] = $this->getSetting('default_value_label_custom');
            break;

          case 'state':
            $transition_labels[$default_value] = $transition_to_state->label();
            break;

          case 'transition':
            // Already set, nothing to do.
            break;

        }
      }
    }
    $element += [
      '#type' => 'container',
      'current' => [
        '#type' => 'item',
        '#title' => $this->t('Current state'),
        '#markup' => $default->label(),
        '#access' => !$entity->isNew(),
        '#wrapper_attributes' => [
          'class' => ['container-inline'],
        ],
      ],
      'state' => [
        '#type' => $this->getSetting('form_element'),
        '#title' => $entity->isNew() ? $this->t('Save as') : $this->t('Transitions'),
        '#key_column' => $this->column,
        '#options' => $transition_labels,
        '#default_value' => $default_value,
        '#access' => !empty($transition_labels),
        '#wrapper_attributes' => [
          'class' => ['container-inline'],
        ],
      ],
    ];

    if (!$this->getSetting('show_default_value_radio')) {
      $element['#after_build'][] = [get_class($this), 'hideDefaultValueRadio'];
    }
    $element['#element_validate'][] = [get_class($this), 'validateElement'];

    return $element;
  }

  /**
   * Hides the radio option for the default value as an #after_build callback.
   *
   * @param array $element
   *   The form element to process.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form element.
   */
  public static function hideDefaultValueRadio(array $element, FormStateInterface $form_state) : array {
    $default_value = $element['state']['#default_value'];
    $element['state'][$default_value]['#wrapper_attributes']['class'][] = 'visually-hidden';
    return $element;
  }

}
